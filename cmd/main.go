package main

import (
	"net"

	"gitlab.com/test-kafka/customerservice/config"
	pb "gitlab.com/test-kafka/customerservice/genproto/customer"
	"gitlab.com/test-kafka/customerservice/kafka"
	"gitlab.com/test-kafka/customerservice/pkg/db"
	"gitlab.com/test-kafka/customerservice/pkg/logger"
	"gitlab.com/test-kafka/customerservice/pkg/messagebroker"
	"gitlab.com/test-kafka/customerservice/service"
	grpcclient "gitlab.com/test-kafka/customerservice/service/grpc_client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "template-service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectTODB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("error while connect to clients revi", logger.Error(err))
	}
	// Kafka
	produceMap := make(map[string]messagebroker.Producer)
	topic := "customer.customer"
	customerTopicProduce := kafka.NewKafkaProducer(cfg, log, topic)
	defer func() {
		err := customerTopicProduce.Stop()
		if err != nil {
			log.Fatal("Failed to stopping Kafka", logger.Error(err))
		}
	}()
	produceMap["customer"] = customerTopicProduce
	// Kafka
	customerService := service.NewCustomerService(grpcClient, connDB, log, produceMap)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listenings: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterCustomerServiceServer(s, customerService)
	log.Info("main: server running",
		logger.String("port", cfg.RPCPort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
