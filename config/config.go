package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment       string //develop, staging, production
	PostgresHost      string
	PostgresPort      int
	PostgresDatabase  string
	PostgresUser      string
	PostgresPassword  string
	LogLevel          string
	RPCPort           string
	PostServiceHost   string
	PostServicePort   int
	ReviewServiceHost string
	ReviewServicePort int
	// KafkaHost         string
	// KafkaPort         int
}

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT",
		"develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST",
		"database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT",
		5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "customerdb1"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "azamali"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "azamali"))
	c.PostServiceHost = cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "post_host"))
	c.PostServicePort = cast.ToInt(getOrReturnDefault("POST_SERVICE_PORT", 9002))

	c.ReviewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "review_host"))
	c.ReviewServicePort = cast.ToInt(getOrReturnDefault("REVIEW_SERVICE_PORT", 9003))

	// c.KafkaHost = cast.ToString(getOrReturnDefault("KAFKA_HOST", "kafka"))
	// c.KafkaPort = cast.ToInt(getOrReturnDefault("KAFKA_PORT", 9092))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":9001"))
	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
