CREATE TABLE IF NOT EXISTS addresses(
    customer_id uuid,
    district TEXT,
    street TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP);