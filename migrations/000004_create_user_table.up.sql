CREATE TABLE IF NOT EXISTS users(
    id uuid NOT NULL,
    firstname VARCHAR(30)not null,
    lastname VARCHAR(30)NOT NULL,
    email TEXT NOT NULL,
    username VARCHAR(30),
    password TEXT NOT NULL,
    refreshtoken TEXT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP);
