package service

import (
	"context"

	pbp "gitlab.com/test-kafka/customerservice/genproto/post"
	l "gitlab.com/test-kafka/customerservice/pkg/logger"
	grpcclient "gitlab.com/test-kafka/customerservice/service/grpc_client"

	// "gitlab.com/test-kafka/customerservice/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// PostService ...
type PostService struct {
	post *grpcclient.ServiceManager
	// storage storage.IStorage
	logger l.Logger
}

func (s *PostService) CreatePost(ctx context.Context, req *pbp.PostReq) (*pbp.PostResp, error) {
	post, err := s.post.PostService().CreatePost(ctx, req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error insert user", err))
		return &pbp.PostResp{}, status.Error(codes.Internal, "something went wrong, please check customer info")
	}
	return post, nil
}

func (s *PostService) GetPostById(ctx context.Context, req *pbp.ID) (*pbp.Post, error) {
	_, err := s.post.PostService().GetPostById(ctx, req)
	if err != nil {
		s.logger.Error("Error while insert", l.Any("error select users", err))
		return &pbp.Post{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	return &pbp.Post{}, nil
}

func (s *PostService) UpdatePost(ctx context.Context, req *pbp.Post) (*pbp.Post, error) {
	res, err := s.post.PostService().UpdatePost(ctx, req)
	if err != nil {
		s.logger.Error("Error while updating", l.Any("Update", err))
		return &pbp.Post{}, status.Error(codes.InvalidArgument, "Please check customer info")
	}
	return res, nil

}

func (s *PostService) DeleteDatabase(ctx context.Context, req *pbp.ID) (*pbp.Empty, error) {
	_, err := s.post.PostService().DeleteDatabase(ctx, req)
	if err != nil {
		s.logger.Error("Error while delete post", l.Any("Delete", err))
		return &pbp.Empty{}, status.Error(codes.InvalidArgument, "wrong id for delete")
	}
	return &pbp.Empty{}, nil
}

// func (s *PostService) GetPosts(ctx context.Context, req *pbp.Empty) (*pbp.PostResp, error) {
// 	_, err := s.post.PostService().GetPostAll(ctx, req)
// 	if err != nil {
// 		s.logger.Error("error while geting customers", l.Any("error getting customers", err))
// 		return &pbp.PostResp{}, status.Error(codes.Internal, "something went wrong")
// 	}
// 	return &pbp.PostResp{}, nil
// }
