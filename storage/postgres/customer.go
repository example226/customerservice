package postgres

import (
	"database/sql"
	"fmt"
	"log"

	pb "gitlab.com/test-kafka/customerservice/genproto/customer"

	// pbp "gitlab.com/test-kafka/customerservice/genproto/post"

	"github.com/jmoiron/sqlx"
)

type customerRepo struct {
	db *sqlx.DB
}

// NewCustomerRepo ...

func NewCustomerRepo(db *sqlx.DB) *customerRepo {
	return &customerRepo{db: db}
}

func (r *customerRepo) CreateUser(user *pb.User) (*pb.User, error) {
	userResp := pb.User{}
	fmt.Println(user)
	err := r.db.QueryRow(`insert into users 
	(id,firstname,lastname,email,username,password,refreshtoken)
	values ($1,$2,$3,$4,$5,$6,$7) returning id,firstname,lastname,
	email,username,password,refreshtoken`,
		user.CustomerUuid, user.FirstName, user.LastName,
		user.Email, user.Username, user.Password, user.Refreshtoken).Scan(
		&userResp.CustomerUuid, &userResp.FirstName, &userResp.LastName,
		&userResp.Email, &userResp.Username, &userResp.Password, &userResp.Refreshtoken)
	if err != nil {
		return &pb.User{}, err
	}
	return &userResp, nil
}

func (r *customerRepo) GetByEmail(user *pb.LoginReq) (*pb.User, error) {
	userResp := pb.User{}
	err := r.db.QueryRow(`select id,firstname,lastname,username,
	password,refreshtoken from 
	users where email=$1`, user.Email).Scan(
		&userResp.CustomerUuid, &userResp.FirstName,
		&userResp.LastName, &userResp.Username,
		&userResp.Password, &userResp.Refreshtoken)
	if err != nil {
		return &pb.User{}, err
	}
	return &userResp, nil

}

func (r *customerRepo) CreateCustomer(customer *pb.CustomerReq) (*pb.CustomerResp, error) {
	customerResp := pb.CustomerResp{}
	fmt.Println(customer)
	err := r.db.QueryRow(`insert into customer 
	(customer_id,firstname,lastname,
		bio,email,password,username,
		phonenumber,refreshtoken) 
	values ($1,$2,$3,$4,$5,$6,$7,$8,$9) returning customer_id,
	firstname,lastname,bio,
	email,password,username,phonenumber`,
		customer.Uuid, customer.FirstName, customer.LastName,
		customer.Bio, customer.Email,
		customer.Password, customer.Username,
		customer.PhoneNumber, customer.Refreshtoken).Scan(
		&customerResp.Uuid, &customerResp.FirstName,
		&customerResp.LastName, &customerResp.Bio,
		&customerResp.Email, &customerResp.Password,
		&customerResp.Username, &customerResp.PhoneNumber)
	if err != nil {
		return &pb.CustomerResp{}, err
	}
	add := []*pb.AddressResp{}
	for _, i := range customer.Addresses {
		addressResp := pb.AddressResp{}
		err := r.db.QueryRow(`insert into addresses (customer_id,district,street) 
		values($1,$2,$3) returning
		customer_id,district,street,id`, customer.Uuid,
			i.District, i.Street).Scan(&addressResp.Uuid,
			&addressResp.District,
			&addressResp.Street,
			&addressResp.Id)
		if err != nil {
			return &pb.CustomerResp{}, err
		}
		add = append(add, &addressResp)
	}
	customerResp.Addresses = add
	return &customerResp, nil
}

func (r *customerRepo) GetCustomerById(ids *pb.ID) (*pb.GetCustomer, error) {
	fmt.Println(ids)
	tempCustomer := &pb.GetCustomer{}
	err := r.db.QueryRow(`select firstname,lastname,
	bio,email,
	phonenumber,customer_id from customer 
	where 
	customer_id=$1 and deleted_at is null`,
		ids.Uuid).Scan(
		&tempCustomer.FirstName,
		&tempCustomer.LastName,
		&tempCustomer.Bio,
		&tempCustomer.Email,
		&tempCustomer.PhoneNumber,
		&tempCustomer.Uuid)
	if err == sql.ErrNoRows {
		fmt.Println("BU 2c hi")
		return &pb.GetCustomer{}, nil
	}
	if err != nil {
		log.Fatal("Error while select customers", err)
		return &pb.GetCustomer{}, err
	}

	rows, err := r.db.Query(`select district,street,customer_id,id from addresses where customer_id=$1`, ids.Uuid)
	if err == sql.ErrNoRows {
		return &pb.GetCustomer{}, nil
	}
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		temp := &pb.Address{}
		err = rows.Scan(
			&temp.District,
			&temp.Street,
			&temp.Uuid,
			&temp.Id,
		)
		if err != nil {
			fmt.Println(err)
		}
		tempCustomer.Addresses = append(tempCustomer.Addresses, temp)
	}

	return tempCustomer, nil
}

func (r *customerRepo) UpdateCustomer(req *pb.Customer) (*pb.Customer, error) {
	customerResp := pb.Customer{}
	err := r.db.QueryRow(`UPDATE customer SET 
	firstname=$1,lastname=$2,bio=$3,
	email=$4,phonenumber=$5 where customer_id=$6 and deleted_at is null returning customer_id,firstname,lastname,bio,email,phonenumber`,
		req.FirstName, req.LastName,
		req.Bio, req.Email,
		req.PhoneNumber, req.Uuid).Scan(
		&customerResp.Uuid, &customerResp.FirstName,
		&customerResp.LastName, &customerResp.Bio,
		&customerResp.Email, &customerResp.PhoneNumber)
	if err == sql.ErrNoRows {
		return &pb.Customer{}, nil
	}
	if err != nil {
		return &pb.Customer{}, err
	}
	for _, address := range req.Addresses {
		addressResp := pb.Address{}
		err = r.db.QueryRow(`UPDATE addresses
		SET district=$1,street=$2 where customer_id=$3 and deleted_at is null returning id,customer_id,district,street`,
			address.District, address.Street, req.Uuid).Scan(
			&addressResp.Id, &addressResp.Uuid,
			&addressResp.District, &addressResp.Street,
		)
		if err != nil {
			return &pb.Customer{}, err
		}
		customerResp.Addresses = append(customerResp.Addresses, &addressResp)
	}
	return &customerResp, nil
}

func (r *customerRepo) DeleteCustomer(ids *pb.ID) error {
	_, err := r.db.Exec(`update customer set deleted_at=NOW()
	where customer_id=$1 and deleted_at is null`, ids.Uuid)
	if err == sql.ErrNoRows {
		return nil
	}
	if err != nil {
		log.Fatal("Error while delete customer", err)
		return err
	}
	return nil
}

func (r *customerRepo) CheckField(req *pb.CheckFieldReq) (*pb.CheckFieldResp, error) {
	query := fmt.Sprintf("SELECT 1 FROM customer WHERE %s=$1", req.Field)
	res := &pb.CheckFieldResp{}
	temp := -1
	err := r.db.QueryRow(query, req.Value).Scan(&temp)
	if err != nil {
		res.Exists = false
		return res, nil
	}
	if temp == 1 {
		res.Exists = true
	} else {
		res.Exists = false
	}
	return res, nil
}

func (r *customerRepo) GetLists(req *pb.ListReq) (*pb.ListResp, error) {
	offset := (req.Page - 1) * req.Limit

	rows, err := r.db.Query(`SELECT firstname,lastname,bio,email,phonenumber,
	customer_id from customer 
	LIMIT $1 OFFSET $2`, req.Page, offset)
	if err == sql.ErrNoRows {
		return &pb.ListResp{}, nil
	}
	if err != nil {
		return &pb.ListResp{}, err
	}
	listCust := &pb.ListResp{}
	for rows.Next() {
		temp := pb.CustomerResp{}
		err = rows.Scan(
			&temp.FirstName,
			&temp.LastName,
			&temp.Bio,
			&temp.Email,
			&temp.PhoneNumber,
			&temp.Uuid,
		)
		if err != nil {
			return &pb.ListResp{}, err
		}
		listCust.Customers = append(listCust.Customers, &temp)
	}
	return listCust, nil

}

func (r *customerRepo) GetAdminByUsername(user *pb.AdminReq) (*pb.AdminResp, error) {
	adminResp := pb.AdminResp{}
	err := r.db.QueryRow(`select id,password,username from 
	admins where username=$1`, user.Username).Scan(
		&adminResp.Id,
		&adminResp.Password,
		&adminResp.Username)
	if err == sql.ErrNoRows {
		return &pb.AdminResp{}, nil
	}

	if err != nil {
		return &pb.AdminResp{}, err
	}
	return &adminResp, nil

}

func (r *customerRepo) GetModeratorByUsername(user *pb.ModeratorReq) (*pb.ModeratorResp, error) {
	moderatorResp := pb.ModeratorResp{}
	err := r.db.QueryRow(`select id,email,password,username,first_name,last_name from 
	moderators where username=$1`, user.Username).Scan(
		&moderatorResp.Id,
		&moderatorResp.Email, &moderatorResp.Password,
		&moderatorResp.Username,
		&moderatorResp.FirstName, &moderatorResp.LastName)

	if err == sql.ErrNoRows {
		return &pb.ModeratorResp{}, nil
	}
	if err != nil {
		return &pb.ModeratorResp{}, err
	}
	return &moderatorResp, nil

}
