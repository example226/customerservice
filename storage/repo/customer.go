package repo

import (
	pb "gitlab.com/test-kafka/customerservice/genproto/customer"
)

// CustomerStorageI ...
type CustomerStorageI interface {
	CreateCustomer(*pb.CustomerReq) (*pb.CustomerResp, error)
	GetCustomerById(*pb.ID) (*pb.GetCustomer, error)
	UpdateCustomer(*pb.Customer) (*pb.Customer, error)
	DeleteCustomer(*pb.ID) error
	CheckField(*pb.CheckFieldReq) (*pb.CheckFieldResp, error)
	GetLists(*pb.ListReq) (*pb.ListResp, error)
	CreateUser(*pb.User) (*pb.User, error)
	GetByEmail(*pb.LoginReq) (*pb.User, error)
	GetAdminByUsername(*pb.AdminReq) (*pb.AdminResp, error)
	GetModeratorByUsername(*pb.ModeratorReq) (*pb.ModeratorResp, error)
}
